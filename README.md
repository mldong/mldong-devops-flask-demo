# mldong-devops-flask-demo

#### 介绍
Flask+Vue前后端分离最佳实践-持续集成demo

#### 软件架构
技术栈
* Python3.6+
* Flask==2.0.2

#### 安装教程
`Git Bash下操作`
1.  拉取代码
```shell
git clone https://gitee.com/mldong/mldong-devops-flask-demo.git
```
2.  进入目录
```shell
cd mldong-devops-flask-demo
```
3.  创建虚拟环境
```shell
python -m venv ./venv
```
4.  激活虚拟环境
```shell
source ./venv/Scripts/activate
```
5.  安装依赖

``` shell
pip3 install -r requirements.txt -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com
```
6.  启动服务
```shell
flask run
# 或者
python app.py
```
## 相关项目

- [mldong-devops-demo](https://gitee.com/mldong/mldong-devops-demo)
