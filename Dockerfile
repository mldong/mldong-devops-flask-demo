FROM registry.cn-zhangjiakou.aliyuncs.com/mldong/python:3.8.12-slim-uwsgi
WORKDIR /app
ADD . /app
RUN pip3 install -r requirements.txt -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com
EXPOSE 5000
CMD ["uwsgi","uwsgi.ini"]
